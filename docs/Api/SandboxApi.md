# Swagger\Client\SandboxApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**testtoolsMerchantIdTestorderPost**](SandboxApi.md#testtoolsMerchantIdTestorderPost) | **POST** /testtools/{merchantId}/testorder/ | Trigger testorders.


# **testtoolsMerchantIdTestorderPost**
> \Swagger\Client\Model\InlineResponse2001 testtoolsMerchantIdTestorderPost($merchant_id, $parameters)

Trigger testorders.

Triggers three (3) testorders by default. Optionally the amount of items to be generated can be specified or randomized.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\SandboxApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$parameters = new \Swagger\Client\Model\Parameters(); // \Swagger\Client\Model\Parameters | Testorder parameters.

try {
    $result = $api_instance->testtoolsMerchantIdTestorderPost($merchant_id, $parameters);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SandboxApi->testtoolsMerchantIdTestorderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **parameters** | [**\Swagger\Client\Model\Parameters**](../Model/Parameters.md)| Testorder parameters. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

