# Swagger\Client\ConfigurationApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet**](ConfigurationApi.md#ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/orderingmode/ | Get the method you are configured to receive orders.
[**ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost**](ConfigurationApi.md#ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orderingmode/ | The merchants endpoint where new orders will be pushed to.


# **ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet**
> \Swagger\Client\Model\InlineResponse2002 ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet($merchant_id)

Get the method you are configured to receive orders.

Ordering mode is set independently from the webhooks

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ConfigurationApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet($merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConfigurationApi->ordermgtTransportsOrderApiV1MerchantIdOrderingmodeGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost**
> \Swagger\Client\Model\InlineResponse2002 ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost($merchant_id, $ordering_mode)

The merchants endpoint where new orders will be pushed to.

Ordering mode is set independently from the webhooks

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ConfigurationApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$ordering_mode = new \Swagger\Client\Model\OrderingMode(); // \Swagger\Client\Model\OrderingMode | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost($merchant_id, $ordering_mode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConfigurationApi->ordermgtTransportsOrderApiV1MerchantIdOrderingmodePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **ordering_mode** | [**\Swagger\Client\Model\OrderingMode**](../Model/OrderingMode.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2002**](../Model/InlineResponse2002.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

