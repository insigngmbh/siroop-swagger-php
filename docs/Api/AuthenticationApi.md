# Swagger\Client\AuthenticationApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordermgtTransportsOrderApiV1ObtainAuthTokenPost**](AuthenticationApi.md#ordermgtTransportsOrderApiV1ObtainAuthTokenPost) | **POST** /ordermgt/transports/order_api/v1/obtain_auth_token/ | Get an authorization token.


# **ordermgtTransportsOrderApiV1ObtainAuthTokenPost**
> \Swagger\Client\Model\InlineResponse200 ordermgtTransportsOrderApiV1ObtainAuthTokenPost($merchant_credentials)

Get an authorization token.

Use your merchant portal credentials to obtain an authentication token to work with the API. Note: Your merchant portal account needs to be configured for API-usage.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AuthenticationApi();
$merchant_credentials = new \Swagger\Client\Model\MerchantCredentials1(); // \Swagger\Client\Model\MerchantCredentials1 | The merchant portal credentials used by the merchant.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1ObtainAuthTokenPost($merchant_credentials);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->ordermgtTransportsOrderApiV1ObtainAuthTokenPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_credentials** | [**\Swagger\Client\Model\MerchantCredentials1**](../Model/MerchantCredentials1.md)| The merchant portal credentials used by the merchant. |

### Return type

[**\Swagger\Client\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

