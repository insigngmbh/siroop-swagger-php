# Swagger\Client\OrdersApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordermgtTransportsOrderApiV1MerchantIdOrdersGet**](OrdersApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/orders/ | Fetch a paginated and filtered collection of orders.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet**](OrdersApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/orders/new/ | Fetch a paginated and filtered collection of orders where the reception has not yet been confirmed by the merchant.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet**](OrdersApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/ | Fetch a specific order.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost**](OrdersApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/received/ | Confirm the reception of the order and provide siroop with the merchant&#39;s order reference.


# **ordermgtTransportsOrderApiV1MerchantIdOrdersGet**
> \Swagger\Client\Model\InlineResponse2001 ordermgtTransportsOrderApiV1MerchantIdOrdersGet($merchant_id, $offset, $limit)

Fetch a paginated and filtered collection of orders.

Returns a paginated and filtered collection of orders.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\OrdersApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$offset = 0; // int | offset
$limit = 50; // int | limit

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersGet($merchant_id, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->ordermgtTransportsOrderApiV1MerchantIdOrdersGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **offset** | **int**| offset | [optional] [default to 0]
 **limit** | **int**| limit | [optional] [default to 50]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet**
> \Swagger\Client\Model\InlineResponse2001 ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet($merchant_id, $offset, $limit)

Fetch a paginated and filtered collection of orders where the reception has not yet been confirmed by the merchant.

Returns a paginated and filtered collection of orders where the reception has not yet been confirmed by the merchant.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\OrdersApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$offset = 0; // int | offset
$limit = 50; // int | limit

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet($merchant_id, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **offset** | **int**| offset | [optional] [default to 0]
 **limit** | **int**| limit | [optional] [default to 50]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet**
> \Swagger\Client\Model\InlineResponse2001Orders ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet($merchant_id, $order_id, $offset, $limit)

Fetch a specific order.

Returns a specific order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\OrdersApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$offset = 0; // int | offset
$limit = 50; // int | limit

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet($merchant_id, $order_id, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **offset** | **int**| offset | [optional] [default to 0]
 **limit** | **int**| limit | [optional] [default to 50]

### Return type

[**\Swagger\Client\Model\InlineResponse2001Orders**](../Model/InlineResponse2001Orders.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost**
> \Swagger\Client\Model\InlineResponse2004 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost($merchant_id, $order_id, $merchant_order_ref)

Confirm the reception of the order and provide siroop with the merchant's order reference.

By confirming the receipt of the order and setting the merchant's order reference the order status is changed to `Received`. siroop will treat orders that were not confirmed by the merchant as new orders which means that they will still be included in the new orders collection (see GET \"/ordermgt/transports/order_api/v1/{merchantId}/orders/new\") and the merchant might be contacted in case the we do not receive an update for an order within a specified time perdiod.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\OrdersApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$merchant_order_ref = new \Swagger\Client\Model\Merchantorderref(); // \Swagger\Client\Model\Merchantorderref | The order reference used by the merchant.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost($merchant_id, $order_id, $merchant_order_ref);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **merchant_order_ref** | [**\Swagger\Client\Model\Merchantorderref**](../Model/Merchantorderref.md)| The order reference used by the merchant. | [optional]

### Return type

[**\Swagger\Client\Model\InlineResponse2004**](../Model/InlineResponse2004.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

