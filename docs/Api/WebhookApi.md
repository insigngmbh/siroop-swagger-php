# Swagger\Client\WebhookApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**merchantWebhookOrderIdPut**](WebhookApi.md#merchantWebhookOrderIdPut) | **PUT** /{merchantWebhook}/{orderId} | The merchants endpoint where new orders will be pushed to.
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksGet**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/ | Get a list of webhooks
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksPost**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksPost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/ | Add new webhook
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete) | **DELETE** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/{webhookUuid}/ | Remove a specific webhook
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/{webhookUuid}/ | Get information about a specific webhook
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch) | **PATCH** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/{webhookUuid}/ | Toggle a specific webhook to inactive
[**ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut**](WebhookApi.md#ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut) | **PUT** /ordermgt/transports/order_api/v1/{merchantId}/webhooks/{webhookUuid}/ | Toggle a specific webhook to active


# **merchantWebhookOrderIdPut**
> \Swagger\Client\Model\InlineResponse201 merchantWebhookOrderIdPut($merchant_webhook, $order_id, $order)

The merchants endpoint where new orders will be pushed to.

The merchant can subscribe to a \"new order\" webhook so that siroop will push new orders to an endpoint that the merchant will host. Note: merchant account needs to be configured for order push and availability of the merchants endpoint might be subject to availability monitoring.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_webhook = "merchant_webhook_example"; // string | The merchant webhook in order to get orders pushed.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$order = new \Swagger\Client\Model\Order1(); // \Swagger\Client\Model\Order1 | 

try {
    $result = $api_instance->merchantWebhookOrderIdPut($merchant_webhook, $order_id, $order);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->merchantWebhookOrderIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_webhook** | **string**| The merchant webhook in order to get orders pushed. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **order** | [**\Swagger\Client\Model\Order1**](../Model/Order1.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksGet**
> \Swagger\Client\Model\InlineResponse2003 ordermgtTransportsOrderApiV1MerchantIdWebhooksGet($merchant_id)

Get a list of webhooks

Get a list of webhooks

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksGet($merchant_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksPost**
> \Swagger\Client\Model\InlineResponse2011 ordermgtTransportsOrderApiV1MerchantIdWebhooksPost($merchant_id, $new_webhook)

Add new webhook

Add new webhook

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$new_webhook = new \Swagger\Client\Model\NewWebhook1(); // \Swagger\Client\Model\NewWebhook1 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksPost($merchant_id, $new_webhook);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **new_webhook** | [**\Swagger\Client\Model\NewWebhook1**](../Model/NewWebhook1.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete**
> ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete($merchant_id, $webhook_uuid)

Remove a specific webhook

Remove a specific webhook

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$webhook_uuid = "webhook_uuid_example"; // string | The merchant webhook uuid.

try {
    $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete($merchant_id, $webhook_uuid);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **webhook_uuid** | **string**| The merchant webhook uuid. |

### Return type

void (empty response body)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet**
> \Swagger\Client\Model\InlineResponse2003 ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet($merchant_id, $webhook_uuid)

Get information about a specific webhook

Get information about a specific webhook

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$webhook_uuid = "webhook_uuid_example"; // string | The merchant webhook uuid.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet($merchant_id, $webhook_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **webhook_uuid** | **string**| The merchant webhook uuid. |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch**
> \Swagger\Client\Model\InlineResponse2003 ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch($merchant_id, $webhook_uuid, $toggle_webhook)

Toggle a specific webhook to inactive

Toggle a specific webhook to inactive

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$webhook_uuid = "webhook_uuid_example"; // string | The merchant webhook uuid.
$toggle_webhook = new \Swagger\Client\Model\ToggleWebhook1(); // \Swagger\Client\Model\ToggleWebhook1 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch($merchant_id, $webhook_uuid, $toggle_webhook);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **webhook_uuid** | **string**| The merchant webhook uuid. |
 **toggle_webhook** | [**\Swagger\Client\Model\ToggleWebhook1**](../Model/ToggleWebhook1.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut**
> \Swagger\Client\Model\InlineResponse2003 ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut($merchant_id, $webhook_uuid, $existing_webhook)

Toggle a specific webhook to active

Toggle a specific webhook to active

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\WebhookApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$webhook_uuid = "webhook_uuid_example"; // string | The merchant webhook uuid.
$existing_webhook = new \Swagger\Client\Model\ExistingWebhook1(); // \Swagger\Client\Model\ExistingWebhook1 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut($merchant_id, $webhook_uuid, $existing_webhook);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WebhookApi->ordermgtTransportsOrderApiV1MerchantIdWebhooksWebhookUuidPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **webhook_uuid** | **string**| The merchant webhook uuid. |
 **existing_webhook** | [**\Swagger\Client\Model\ExistingWebhook1**](../Model/ExistingWebhook1.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2003**](../Model/InlineResponse2003.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

