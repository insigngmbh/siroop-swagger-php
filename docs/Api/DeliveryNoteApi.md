# Swagger\Client\DeliveryNoteApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost**](DeliveryNoteApi.md#ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/deliverynote/ | Receive delivery note for a specific orderId or itemIds


# **ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost**
> \Swagger\Client\Model\DeliveryNoteResponse ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost($merchant_id, $delivery_note)

Receive delivery note for a specific orderId or itemIds

Provide specific `itemIds` with your request for partial shipments, otherwise you will receive a delivery note for the complete order.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\DeliveryNoteApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$delivery_note = new \Swagger\Client\Model\DeliveryNoteRequest(); // \Swagger\Client\Model\DeliveryNoteRequest | Specifying itemIds are optional in case of partial orders.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost($merchant_id, $delivery_note);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveryNoteApi->ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **delivery_note** | [**\Swagger\Client\Model\DeliveryNoteRequest**](../Model/DeliveryNoteRequest.md)| Specifying itemIds are optional in case of partial orders. |

### Return type

[**\Swagger\Client\Model\DeliveryNoteResponse**](../Model/DeliveryNoteResponse.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

