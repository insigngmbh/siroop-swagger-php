# Swagger\Client\ItemsApi

All URIs are relative to *https://merchants-sandbox.siroop.ch*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/accept/ | Optional: Accept the fulfillment of a single order item.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/cancel/ | Cancel a single order item.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/decline/ | Decline a single order item.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/delivery-date/ | Update expected delivery date.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet) | **GET** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/ | Fetch a single order item.
[**ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost**](ItemsApi.md#ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost) | **POST** /ordermgt/transports/order_api/v1/{merchantId}/orders/{orderId}/items/{itemId}/shipped/ | Provide shipment information for a single order item.


# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost**
> \Swagger\Client\Model\InlineResponse2005 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost($merchant_id, $order_id, $item_id)

Optional: Accept the fulfillment of a single order item.

Note: Strictly implies that the communicated delivery date matches the merchants expected delivery date. This sets the item status to \"Confirmed\"

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost($merchant_id, $order_id, $item_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |

### Return type

[**\Swagger\Client\Model\InlineResponse2005**](../Model/InlineResponse2005.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost**
> \Swagger\Client\Model\InlineResponse2007 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost($merchant_id, $order_id, $item_id, $message)

Cancel a single order item.

Note: A cancel is necessary if a single order item that was already accepted cannot be delivered. This sets the item status to \"Canceled\"

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.
$message = new \Swagger\Client\Model\Message1(); // \Swagger\Client\Model\Message1 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost($merchant_id, $order_id, $item_id, $message);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |
 **message** | [**\Swagger\Client\Model\Message1**](../Model/Message1.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2007**](../Model/InlineResponse2007.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost**
> \Swagger\Client\Model\InlineResponse2006 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost($merchant_id, $order_id, $item_id, $message)

Decline a single order item.

Note: The decline is only valid for single order items in the \"NEW\" status e.g. before it was accepted. This sets the item status to \"Declined\"

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.
$message = new \Swagger\Client\Model\Message(); // \Swagger\Client\Model\Message | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost($merchant_id, $order_id, $item_id, $message);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |
 **message** | [**\Swagger\Client\Model\Message**](../Model/Message.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2006**](../Model/InlineResponse2006.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost**
> \Swagger\Client\Model\InlineResponse2008 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost($merchant_id, $order_id, $item_id, $message)

Update expected delivery date.

If not shipped or canceled, the delivery date can be updated multiple times. This does not impact the status of an item.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.
$message = new \Swagger\Client\Model\Message2(); // \Swagger\Client\Model\Message2 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost($merchant_id, $order_id, $item_id, $message);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |
 **message** | [**\Swagger\Client\Model\Message2**](../Model/Message2.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2008**](../Model/InlineResponse2008.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet**
> \Swagger\Client\Model\InlineResponse2001 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet($merchant_id, $order_id, $item_id, $offset, $limit)

Fetch a single order item.

Returns a single order item.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.
$offset = 0; // int | offset
$limit = 50; // int | limit

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet($merchant_id, $order_id, $item_id, $offset, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |
 **offset** | **int**| offset | [optional] [default to 0]
 **limit** | **int**| limit | [optional] [default to 50]

### Return type

[**\Swagger\Client\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost**
> \Swagger\Client\Model\InlineResponse2009 ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost($merchant_id, $order_id, $item_id, $message)

Provide shipment information for a single order item.

Providing a valid carrier and tracking number is mandatory. This sets the item status to \"Shipped\"

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$api_instance = new Swagger\Client\Api\ItemsApi();
$merchant_id = "merchant_id_example"; // string | The merchant-id of a specific merchant.
$order_id = "order_id_example"; // string | The siroop order identifier (alphanumeric).
$item_id = "item_id_example"; // string | The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item.
$message = new \Swagger\Client\Model\Message3(); // \Swagger\Client\Model\Message3 | 

try {
    $result = $api_instance->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost($merchant_id, $order_id, $item_id, $message);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **merchant_id** | **string**| The merchant-id of a specific merchant. |
 **order_id** | **string**| The siroop order identifier (alphanumeric). |
 **item_id** | **string**| The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. |
 **message** | [**\Swagger\Client\Model\Message3**](../Model/Message3.md)|  |

### Return type

[**\Swagger\Client\Model\InlineResponse2009**](../Model/InlineResponse2009.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

