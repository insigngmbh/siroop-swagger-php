# InlineResponse2001Trackings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **string** | The carrier used by the merchant for a shipment. | 
**code** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


