# MerchantOrderReference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_order_ref** | **string** | reference id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


