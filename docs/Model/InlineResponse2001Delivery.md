# InlineResponse2001Delivery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pickup** | **bool** | TRUE in case of delivery to a pickup station, FALSE otherwise. | [optional] [default to false]
**shipment_type** | **string** | Currently, the shipment types PARCEL, HAULIER, and VINOLOG (for wine deliveries) are supported. | 
**delivery_address** | [**\Swagger\Client\Model\InlineResponse2001DeliveryDeliveryAddress**](InlineResponse2001DeliveryDeliveryAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


