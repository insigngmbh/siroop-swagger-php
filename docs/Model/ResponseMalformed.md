# ResponseMalformed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dev_message** | **string** | Malformed request | [optional] 
**message** | **string** | Malformed request | [optional] 
**property** | **object** |  | [optional] 
**status** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


