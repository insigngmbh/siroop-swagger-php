# ItemCancelResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** |  | [optional] 
**dev_message** | **string** |  | [optional] 
**message** | **string** | Item O2W3R-44561-82959-01 cancelled with reason 6_other | [optional] 
**code** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


