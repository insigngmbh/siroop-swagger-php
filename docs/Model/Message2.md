# Message2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reason** | **string** |  | [optional] 
**partial_delivery** | **bool** | significance: delayed order item will be shipped separately once available. other items - as far as not delayed as well - will be shipped according to communicated delivery date. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


