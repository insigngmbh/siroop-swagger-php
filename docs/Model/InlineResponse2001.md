# InlineResponse2001

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**next** | **string** |  | [optional] 
**previous** | **string** |  | [optional] 
**orders** | [**\Swagger\Client\Model\InlineResponse2001Orders[]**](InlineResponse2001Orders.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


