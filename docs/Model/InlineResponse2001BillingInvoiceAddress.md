# InlineResponse2001BillingInvoiceAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** |  | 
**company** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**given_name** | **string** |  | 
**house_number** | **string** |  | 
**last_name** | **string** |  | 
**salutation** | **string** |  | [optional] 
**street_name** | **string** |  | 
**zip** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


