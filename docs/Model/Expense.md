# Expense

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** | Currency of the price. Currently only “CHF” allowed. | [optional] 
**gross_price** | **float** | Price in cents including VAT. | [optional] 
**type** | **string** | shipment_compensation_for_merchant | [optional] 
**vat** | **float** | Value added tax | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


