# InlineResponse2011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Malformed request | [optional] 
**active** | **bool** | Enable and disable webhook | [optional] 
**url** | **string** | Merchants Webhook URL | [optional] 
**token** | **string** | Merchants Secure Token | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


