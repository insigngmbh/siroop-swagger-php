# InlineResponse2001Billing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_address** | [**\Swagger\Client\Model\InlineResponse2001BillingInvoiceAddress**](InlineResponse2001BillingInvoiceAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


