# Parameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**random_amount_of_products** | **bool** | testorder | [optional] 
**number_of_items** | **int** | number of items | [optional] 
**order_type** | **string** |  | [optional] 
**product_ids** | [**null[]**](.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


