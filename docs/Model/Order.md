# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basket_reference** | **string** | Reference to the siroop shop basket. | 
**billing** | [**\Swagger\Client\Model\InlineResponse2001Billing**](InlineResponse2001Billing.md) |  | [optional] 
**communicated_delivery_date** | [**\DateTime**](\DateTime.md) | The original and immutable delivery date communicated to the customer. | 
**expected_delivery_date** | [**\DateTime**](\DateTime.md) | The updated delivery date communicated by the merchant. | [optional] 
**customer_id** | **string** | A reference to a unique customer account. Note, the same person can create multiple client ids and so can have multiple customer ids. | [optional] 
**customer_language** | **string** | The preferred language of the customer. | 
**delivery** | [**\Swagger\Client\Model\InlineResponse2001Delivery**](InlineResponse2001Delivery.md) |  | [optional] 
**expenses** | [**\Swagger\Client\Model\InlineResponse2001Expenses[]**](InlineResponse2001Expenses.md) |  | [optional] 
**merchant_id** | **string** | The merchant id | [optional] 
**merchant_order_reference** | **string** | The order reference used by the merchant. | [optional] 
**order_date** | [**\DateTime**](\DateTime.md) | Time when the order was placed by the customer. | 
**order_id** | **string** | The unique siroop id for the merchant order. | 
**single_order_items** | [**\Swagger\Client\Model\InlineResponse2001SingleOrderItems[]**](InlineResponse2001SingleOrderItems.md) |  | [optional] 
**status** | **string** | The status of the order. | 
**test_order** | **bool** | Indicates if the order is a testorder or not. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


