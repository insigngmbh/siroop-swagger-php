# ResponseUnauthorized

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** |  | [optional] 
**dev_message** | **string** | AuthenticationFailed | [optional] 
**message** | **string** | Invalid token. | [optional] 
**detail** | **string** | Invalid token. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


