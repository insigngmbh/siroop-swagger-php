# ItemShippedMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trackings** | [**\Swagger\Client\Model\InlineResponse2001Trackings[]**](InlineResponse2001Trackings.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


