# ItemDeliveryDateUpdateResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** |  | [optional] 
**dev_message** | **string** |  | [optional] 
**message** | **string** | Updated delivery-date for item 3Y7G1-44561-52113-01 to 2017-03-29 00:00:00+00:00 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


