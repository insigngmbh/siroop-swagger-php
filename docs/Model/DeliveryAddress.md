# DeliveryAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** |  | 
**company** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**given_name** | **string** |  | 
**house_number** | **string** |  | 
**last_name** | **string** |  | 
**salutation** | **string** |  | [optional] 
**street_name** | **string** |  | 
**zip** | **string** |  | 
**additional_info** | **string** | Additional information to the delivery address. | [optional] 
**siroop_reference** | **string** | siroop reference number, must be printed on the packet label in case of delivery to a pickup station, e.g. siroop ZGH123. | [optional] 
**contact_hours** | **string** | Mandatory in case of hauling. It indicates when the customer is available on the phone for the haulier to arrange delivery, e.g. 08:00-10:00 | [optional] 
**phone** | **string** | Mandatory in case of hauling. Phone number of the delivery contact person. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


