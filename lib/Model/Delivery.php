<?php
/**
 * Delivery
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * siroop Order API Sandbox
 *
 * The siroop Merchant Order API Sandbox allows merchants to test their order handling service in a non-productive playground. Automated order handling is an exciting step forward towards making it easier for merchants to integrate with the siroop marketplace and thus handle siroop orders directly from their own systems. You can find out more about the siroop API's at [http://developer.siroop.ch](http://developer.siroop.ch).
 *
 * OpenAPI spec version: 1.0.8
 * Contact: b2b@siroop.ch
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * Delivery Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class Delivery implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'delivery';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'pickup' => 'bool',
        'shipment_type' => 'string',
        'delivery_address' => '\Swagger\Client\Model\InlineResponse2001DeliveryDeliveryAddress'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'pickup' => null,
        'shipment_type' => null,
        'delivery_address' => null
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'pickup' => 'Pickup',
        'shipment_type' => 'shipmentType',
        'delivery_address' => 'deliveryAddress'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'pickup' => 'setPickup',
        'shipment_type' => 'setShipmentType',
        'delivery_address' => 'setDeliveryAddress'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'pickup' => 'getPickup',
        'shipment_type' => 'getShipmentType',
        'delivery_address' => 'getDeliveryAddress'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const SHIPMENT_TYPE_PARCEL = 'PARCEL';
    const SHIPMENT_TYPE_HAULIER = 'HAULIER';
    const SHIPMENT_TYPE_VINOLOG = 'VINOLOG';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getShipmentTypeAllowableValues()
    {
        return [
            self::SHIPMENT_TYPE_PARCEL,
            self::SHIPMENT_TYPE_HAULIER,
            self::SHIPMENT_TYPE_VINOLOG,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['pickup'] = isset($data['pickup']) ? $data['pickup'] : false;
        $this->container['shipment_type'] = isset($data['shipment_type']) ? $data['shipment_type'] : null;
        $this->container['delivery_address'] = isset($data['delivery_address']) ? $data['delivery_address'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['shipment_type'] === null) {
            $invalid_properties[] = "'shipment_type' can't be null";
        }
        $allowed_values = $this->getShipmentTypeAllowableValues();
        if (!in_array($this->container['shipment_type'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'shipment_type', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['shipment_type'] === null) {
            return false;
        }
        $allowed_values = $this->getShipmentTypeAllowableValues();
        if (!in_array($this->container['shipment_type'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets pickup
     * @return bool
     */
    public function getPickup()
    {
        return $this->container['pickup'];
    }

    /**
     * Sets pickup
     * @param bool $pickup TRUE in case of delivery to a pickup station, FALSE otherwise.
     * @return $this
     */
    public function setPickup($pickup)
    {
        $this->container['pickup'] = $pickup;

        return $this;
    }

    /**
     * Gets shipment_type
     * @return string
     */
    public function getShipmentType()
    {
        return $this->container['shipment_type'];
    }

    /**
     * Sets shipment_type
     * @param string $shipment_type Currently, the shipment types PARCEL, HAULIER, and VINOLOG (for wine deliveries) are supported.
     * @return $this
     */
    public function setShipmentType($shipment_type)
    {
        $allowed_values = $this->getShipmentTypeAllowableValues();
        if (!in_array($shipment_type, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'shipment_type', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['shipment_type'] = $shipment_type;

        return $this;
    }

    /**
     * Gets delivery_address
     * @return \Swagger\Client\Model\InlineResponse2001DeliveryDeliveryAddress
     */
    public function getDeliveryAddress()
    {
        return $this->container['delivery_address'];
    }

    /**
     * Sets delivery_address
     * @param \Swagger\Client\Model\InlineResponse2001DeliveryDeliveryAddress $delivery_address
     * @return $this
     */
    public function setDeliveryAddress($delivery_address)
    {
        $this->container['delivery_address'] = $delivery_address;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


