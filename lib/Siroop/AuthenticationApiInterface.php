<?php

namespace Swagger\Client\Siroop;


interface AuthenticationApiInterface {

    /**
     * Obtain an authentication token.
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\InlineResponse200
     */
    function authenticate();
    
}
