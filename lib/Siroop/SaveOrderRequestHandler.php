<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\ApiException;
use Swagger\Client\Model\Order;

/**
 * Handles a call to the siroop Order save Webhook API. URL should be `PUT /api/v1/siroop/order`.
 *
 * @package Swagger\Client\Siroop
 */
class SaveOrderRequestHandler {

    /** @var string The request body */
    private $requestBody = NULL;

    /** @var OrderMapperInterface */
    private $mapper;

    /**
     * SaveOrderRequestHandler constructor.
     *
     * @param OrderMapperInterface $mapper
     */
    public function __construct(
        OrderMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * Handles an API call.
     *
     * @return \Swagger\Client\Model\OrderResponseMessage
     * @throws ApiException
     */
    public function run() {
        if($_SERVER['REQUEST_METHOD'] !== 'PUT')
            throw new ApiException("Only HTTP method `PUT` is allowed.", 100);

        try {
            $api = new WebhookApi();
            return $api->saveOrder($this->mapJson(), $this->mapper);
        } catch(\Exception $e) {
            throw new ApiException("An error occurred while saving order in module: " . $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Turns a JSON object into an {@see \Swagger\Client\Model\Order}.
     *
     * @return Order
     * @throws ApiException
     */
    private function mapJson() {
        try {
            $mapper = new \JsonMapper();
            $order = $mapper->map($this->fetchJson(), new Order());
            if(!($order instanceof Order))
                throw new ApiException("Could not be converted to an Order.", 200);
        } catch(\Exception $e) {
            throw new ApiException("An error occurred while mapping JSON object: " . $e->getMessage(), $e->getCode());
        }

        return $order;
    }

    /**
     * Reads the request body of the HTTP request - this may be required on some systems,
     * as the request body cannot always be read twice.
     *
     * @return string The request body
     */
    public function getRequestBody() {
        if($this->requestBody === NULL) {
            $this->requestBody = @file_get_contents('php://input');
        }
        return $this->requestBody;
    }

    /**
     * Reads the JSON string from the HTTP PUT body and decodes it. JSON object must contain a root node called `order`.
     *
     * @return \stdClass
     * @throws ApiException
     */
    private function fetchJson() {
        try {
            $input = $this->getRequestBody();
            $json = json_decode($input);
        } catch(\Exception $e) {
            throw new ApiException("An error occurred while fetching JSON object: " . $e->getMessage(), $e->getCode());
        }

        return $json;
    }

}
