<?php

namespace Swagger\Client\Siroop;

interface OrderItemApiInterface {

    /**
     * Fetch a single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\SingleOrderItem
     */
    function get($orderId, $itemId);

    /**
     * Set a single order item to ACCEPT.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function accept($orderId, $itemId);

    /**
     * Decline single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message $message  (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function decline($orderId, $itemId, \Swagger\Client\Model\Message $message);

    /**
     * Update expected delivery date.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message2 $message  (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function updateDeliveryDate($orderId, $itemId, \Swagger\Client\Model\Message2 $message);

    /**
     * Cancel single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param  \Swagger\Client\Model\Message1 $message  (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function cancel($orderId, $itemId, \Swagger\Client\Model\Message1 $message);

    /**
     * Provide shipment information for a single order item and set the item to the corresponding status.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message3 $message  (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function shipped($orderId, $itemId, \Swagger\Client\Model\Message3 $message);

}
