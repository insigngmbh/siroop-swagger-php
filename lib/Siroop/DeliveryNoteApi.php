<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Api\DeliveryNoteApi as SiroopDeliveryApi;
use Swagger\Client\ApiException;
use Swagger\Client\ApiClient;

class DeliveryNoteApi implements DeliveryNoteApiInterface {


    /** @var DeliveryNoteApi */
    private $underlying;

    /** @var string */
    private $merchantId;

    /**
     * DeliveryNoteApi constructor.
     * @param null $merchantId
     * @param null $apiKey
     * @param null $apiKeyPrefix
     * @param null $apiBaseUrl
     */
    public function __construct($merchantId = null, $apiKey = null, $apiKeyPrefix = null, $apiBaseUrl = null) {
        if(!$merchantId)
            throw new ApiException("The merchantId must be set.");
        if(!$apiKey)
            throw new ApiException("The apiKey must be set.");
        if(!$apiKeyPrefix)
            throw new ApiException("The apiKeyPrefix must be set.");

        $apiClient = new ApiClient();

        if ($apiBaseUrl) {
            $apiClient->getConfig()->setHost($apiBaseUrl);
        }
        $apiClient->getConfig()->setApiKey('Authorization', $apiKey);
        $apiClient->getConfig()->setApiKeyPrefix('Authorization', $apiKeyPrefix);

        $this->underlying = new SiroopDeliveryApi($apiClient);
        $this->merchantId = $merchantId;
    }

    /**
     * Returns base 64 encoded siroop delivery note/slip
     *
     * @param \Swagger\Client\Model\DeliveryNoteRequest $deliveryNoteRequest
     * @return \Swagger\Client\Model\DeliveryNoteResponse
     */
    function fetchDeliveryNote(\Swagger\Client\Model\DeliveryNoteRequest $deliveryNoteRequest) {
        return $this->underlying->ordermgtTransportsOrderApiV1MerchantIdDeliverynotePost($this->merchantId, $deliveryNoteRequest);
    }

}
