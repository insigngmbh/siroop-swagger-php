<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Api\OrdersApi;
use Swagger\Client\ApiClient;
use Swagger\Client\ApiException;
use Swagger\Client\Model\Order;

class OrderApi implements OrderApiInterface {

    /** @var OrdersApi */
    private $underlying;

    /** @var string */
    private $merchantId;

    /**
     * @param string $merchantId
     * @param string $apiKey
     * @param string $apiKeyPrefix
     * @throws ApiException if $merchantId or $apiKey or $apiKeyPrefix are not provided
     */
    public function __construct($merchantId = null, $apiKey = null, $apiKeyPrefix = null) {
        if(!$merchantId)
            throw new ApiException("The merchantId must be set.");
        if(!$apiKey)
            throw new ApiException("The apiKey must be set.");
        if(!$apiKeyPrefix)
            throw new ApiException("The apiKeyPrefix must be set.");

        $apiClient = new ApiClient();
        $apiClient->getConfig()->setApiKey('Authorization', $apiKey);
        $apiClient->getConfig()->setApiKeyPrefix('Authorization', $apiKeyPrefix);

        $this->underlying = new OrdersApi($apiClient);
        $this->merchantId = $merchantId;
    }

    /**
     * Fetch a paginated and filtered collection of orders.
     *
     * @param int $offset offset (optional, default to 0)
     * @param int $limit limit (optional, default to 50)
     * @param string $status Orders filtered by status (optional)
     * @param string[] $sort (optional)
     * @param \DateTime $after Orders created on or after the specified date (ISO 8601, e.g. 2016-04-30T21:20:50.52Z) (optional)
     * @param \DateTime $before Orders created before the specified date (ISO 8601, e.g. 2016-04-30T21:20:50.52Z) (optional)
     * @param string $client Items ordered by a specific client id. (optional)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PagedOrders
     */
    function get($offset = null, $limit = null) {
        return $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersGet($this->merchantId, $offset, $limit);
    }

    /**
     * Fetch a specific order.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return Order
     */
    function getById($orderId) {
        return $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdGet($this->merchantId, $orderId);
    }

    /**
     * Fetch new orders.
     *
     * @param int $limit limit (optional, default to 50)
     * @param int $offset offset (optional, default to 0)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PagedOrders
     */
    function fetchNew($limit = null, $offset = null) {
        return $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersNewGet($this->merchantId, $limit, $offset);
    }

    /**
     * Confirm the receipt of the order and provide siroop with the merchant's order reference.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $merchantOrderReference The order reference used by the merchant. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function confirm($orderId, $merchantOrderReference) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdReceivedPost($this->merchantId, $orderId, $merchantOrderReference);
    }

}
