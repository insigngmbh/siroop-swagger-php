<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Api\AuthenticationApi as AuthApi;
use Swagger\Client\Model\MerchantCredentials1;
use Swagger\Client\ApiException;
use Swagger\Client\ApiClient;

class AuthenticationApi implements AuthenticationApiInterface {


    /** @var AuthenticationApi */
    private $underlying;

    /** @var MerchantCredentials1 */
    private $credentials;

    /**
     * @param string $merchantId
     * @param null $apiBaseUrl
     * @throws ApiException if $merchant_credentials are not provided
     */
    public function __construct($merchant_credentials, $apiBaseUrl = null) {
        if(!$merchant_credentials)
            throw new ApiException("The merchant credentials must be set.");

        $apiClient = new ApiClient();
        if ($apiBaseUrl) {
            $apiClient->getConfig()->setHost($apiBaseUrl);
        }

        $this->underlying = new AuthApi($apiClient);
        $this->credentials = new MerchantCredentials1($merchant_credentials);
    }
    /**
     * Obtain an authentication token.
     *
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\InlineResponse200
     */
    function authenticate() {
        return $this->underlying->ordermgtTransportsOrderApiV1ObtainAuthTokenPost($this->credentials);
    }

}
