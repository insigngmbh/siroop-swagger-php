<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Model\Order;
use Swagger\Client\Model\OrderResponseMessage;

class WebhookApi implements WebhookApiInterface {

    /**
     * New order is pushed to merchant via an endpoint hosted by the merchant. Route should be
     * `PUT /api/siroop/v1/orders/{order-id}`.
     *
     * @param Order $order (required)
     * @param OrderMapperInterface $mapper (required)
     * @return \Swagger\Client\Model\OrderResponseMessage on non-2xx response
     * @throws \Swagger\Client\ApiException if something went wrong
     */
    public function saveOrder(Order $order, OrderMapperInterface $mapper) {
        return new OrderResponseMessage([
            'merchant_order_ref' => $mapper->map($order)
        ]);
    }

}
