<?php

namespace Swagger\Client\Siroop;

interface DeliveryNoteApiInterface {

    /**
     * Returns base 64 encoded siroop delivery note/slip
     *
     * @param \Swagger\Client\Model\DeliveryNoteRequest $deliveryNoteRequest
     * @return \Swagger\Client\Model\DeliveryNoteResponse
     */
    function fetchDeliveryNote(\Swagger\Client\Model\DeliveryNoteRequest $deliveryNoteRequest);
    
}
