<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Model\Order;

interface OrderApiInterface {

    /**
     * Fetch a paginated and filtered collection of orders.
     *
     * @param int $offset offset (optional, default to 0)
     * @param int $limit limit (optional, default to 50)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PagedOrders
     */
    function get($offset = null, $limit = null);

    /**
     * Fetch a specific order.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return Order
     */
    function getById($orderId);

    /**
     * Fetch new orders.
     *
     * @param int $limit limit (optional, default to 50)
     * @param int $offset offset (optional, default to 0)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\PagedOrders
     */
    function fetchNew($limit = null, $offset = null);

    /**
     * Confirm the receipt of the order and provide siroop with the merchant's order reference.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $merchantOrderReference The order reference used by the merchant. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function confirm($orderId, $merchantOrderReference);

}
