<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Model\Order;

interface OrderMapperInterface {

    /**
     * Converts an order received by the siroop API to a Magento Order and saves it in the database. Will return the new
     * order's ID on success or throw an exception in case an error occurred.
     *
     * @param Order $order The API Order to convert to a Magento Order
     * @return int
     * @throws \Exception if something went wrong
     */
    function map(Order $order);

}
