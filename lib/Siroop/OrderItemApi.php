<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Api\ItemsApi;
use Swagger\Client\ApiClient;
use Swagger\Client\ApiException;

class OrderItemApi implements OrderItemApiInterface {

    /** @var ItemsApi */
    private $underlying;

    /** @var string */
    private $merchantId;

    /**
     * @param string $merchantId
     * @param string $apiKey
     * @param string $apiKeyPrefix
     * @param null $apiBaseUrl
     * @throws ApiException if $merchantId or $apiKey or $apiKeyPrefix or  are not provided
     */
    public function __construct($merchantId = null, $apiKey = null, $apiKeyPrefix = null, $apiBaseUrl = null) {
        if(!$merchantId)
            throw new ApiException("The merchantId must be set.");
        if(!$apiKey)
            throw new ApiException("The apiKey must be set.");
        if(!$apiKeyPrefix)
            throw new ApiException("The apiKeyPrefix must be set.");

        $apiClient = new ApiClient();
        
        if ($apiBaseUrl) {
            $apiClient->getConfig()->setHost($apiBaseUrl);
        }
        $apiClient->getConfig()->setApiKey('Authorization', $apiKey);
        $apiClient->getConfig()->setApiKeyPrefix('Authorization', $apiKeyPrefix);

        $this->underlying = new ItemsApi($apiClient);
        $this->merchantId = $merchantId;
    }

    /**
     * Fetch a single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return \Swagger\Client\Model\SingleOrderItem
     */
    function get($orderId, $itemId) {
        return $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdGet($this->merchantId, $orderId, $itemId);
    }

    /**
     * Set a single order item to ACCEPT.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function accept($orderId, $itemId) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdAcceptPost($this->merchantId, $orderId, $itemId);
    }

    /**
     * Decline single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message $message (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function decline($orderId, $itemId, \Swagger\Client\Model\Message $message) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeclinePost($this->merchantId, $orderId, $itemId, $message);
    }

    /**
     * Update expected delivery date.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message2 $message (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function updateDeliveryDate($orderId, $itemId, \Swagger\Client\Model\Message2 $message) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdDeliveryDatePost($this->merchantId, $orderId, $itemId, $message);
    }

    /**
     * Cancel single order item.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message1 $message (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function cancel($orderId, $itemId, \Swagger\Client\Model\Message1 $message) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdCancelPost($this->merchantId, $orderId, $itemId, $message);
    }

    /**
     * Provide shipment information for a single order item and set the item to the corresponding status.
     *
     * @param string $orderId The siroop order identifier (alphanumeric). (required)
     * @param string $itemId The identifier of a single order item. Can take different shapes e.g. in the future the serial number of a device could be used as id of a single order item. (required)
     * @param \Swagger\Client\Model\Message3 $message (required)
     * @throws \Swagger\Client\ApiException on non-2xx response
     * @return void
     */
    function shipped($orderId, $itemId, \Swagger\Client\Model\Message3 $message) {
        $this->underlying->ordermgtTransportsOrderApiV1MerchantIdOrdersOrderIdItemsItemIdShippedPost($this->merchantId, $orderId, $itemId, $message);
    }
}
