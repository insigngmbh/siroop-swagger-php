<?php

namespace Swagger\Client\Siroop;

use Swagger\Client\Model\Order;

/**
 * Implement this interface in the module providing the Webhook endpoint. The framework should handle routing and the
 * necessary controller actions to process these methods.
 *
 * @package Swagger\Client\Siroop
 */
interface WebhookApiInterface {

    /**
     * New order is pushed to merchant via an endpoint hosted by the merchant. Route should be
     * `PUT /api/siroop/v1/orders/{order-id}`.
     *
     * @param Order $order (required)
     * @param OrderMapperInterface $mapper (required)
     * @return \Swagger\Client\Model\OrderResponseMessage on non-2xx response
     * @throws \Swagger\Client\ApiException if something went wrong
     */
    function saveOrder(Order $order, OrderMapperInterface $mapper);

}
